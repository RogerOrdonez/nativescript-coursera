import { Injectable } from '@angular/core';
import { getJSON, request } from "tns-core-modules/http";

@Injectable({
  providedIn: 'root'
})
export class NewsServiceService {

  private news: Array<string> = [];
  api: string = "https://a3e3be78.ngrok.io";

  constructor() { 
    // Init your component properties here.
  }

  agregar(s: string) {
    this.news.splice(0, 0, s);
    return request({
      url: this.api + "/favs",
      method: "POST",
      headers: {"Content-Type": "application/json"},
      content: JSON.stringify({
        nuevo: s
      })
    });
  }

  favs() {
    return getJSON(this.api + "/favs");
  }

  buscar(s: string) {
    return getJSON(this.api + "/get?q=" + s);
  }

}
