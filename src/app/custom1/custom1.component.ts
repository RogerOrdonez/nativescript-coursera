import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-custom1',
  templateUrl: './custom1.component.html',
  styleUrls: ['./custom1.component.css'],
  moduleId: module.id,
})
export class Custom1Component implements OnInit {

  constructor(private routerExtensions: RouterExtensions) { }

  ngOnInit() {
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onDrawerButtonClick(): void {    
    this.routerExtensions.navigate(["/custom2"], {
      transition: {
          name: "fade"
      }
  });
  }

}
