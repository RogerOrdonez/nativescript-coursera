import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
let LS = require("nativescript-localstorage");

@Component({
    selector: "Settings",
    moduleId: module.id,
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    nombreUsuario: string;

    constructor() {
        // Use the component constructor to inject providers.
        this.nombreUsuario = LS.getItem("nombreUsuario");
        console.log(LS.getItem("nombreUsuario"));
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onTapButton(): void {
        console.log(this.nombreUsuario);
        LS.setItem("nombreUsuario", this.nombreUsuario);
    }
}
