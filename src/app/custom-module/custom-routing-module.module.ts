import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Custom1Component } from "../custom1/custom1.component";
import { Custom2Component } from "../custom2/custom2.component";

const routes: Routes = [
    { path: "", redirectTo: "/custom1", pathMatch: "full" },
    { path: "custom1", component: Custom1Component },
    { path: "custom2", component: Custom2Component }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CustomRoutingModule { }
