import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { CustomRoutingModule } from "./custom-routing-module.module";
import { Custom1Component } from "../custom1/custom1.component";
import { Custom2Component } from "../custom2/custom2.component";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    CustomRoutingModule
  ],
  declarations: [
    Custom1Component,
    Custom2Component
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CustomModuleModule { }
